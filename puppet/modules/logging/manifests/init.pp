class logging {

  package { "rsyslog" :
    ensure => present
  }

  package { "logrotate" :
    ensure => present
  }

  service { "rsyslog" :
    ensure => running
  }

  file { "/etc/rsyslog.conf" :
    ensure => file,
    mode => 0644,
    owner => "root",
    group => "root",
    content => template("logging/etc_rsyslog.conf.erb"),
    notify => Service["rsyslog"]
  }
}

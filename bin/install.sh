#!/bin/bash
add-apt-repository -y ppa:vbernat/haproxy-1.5
apt-get update
apt-get -y install puppet
BASEDIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/..
rsync -Cavz $BASEDIR/puppet/ /etc/puppet/
mv /etc/puppet/site.pp.template /etc/puppet/site.pp
